import {ADD_AUTH_DATA} from "../actionTypes";

export function setCurrentAuthData(userInfo, idToken, accessToken, authenticated, isLoginPending) {
  return{
   type: ADD_AUTH_DATA,
   payload: {userInfo, idToken, accessToken, authenticated, isLoginPending}
  }
}