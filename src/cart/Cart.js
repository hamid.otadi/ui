import React, {useCallback, useEffect, useState} from 'react';
import Item from './CartItem';
import Cookies from 'js-cookie';
import conf from '../config';
import util from '../util';
import {connect} from 'react-redux';
import {Button, Col, Container, Row} from 'react-bootstrap';
import {useHistory} from 'react-router-dom';
import {setCurrentCartItemNumber} from '../redux/actions/cartItemNumber';
import {useAuth} from 'oidc-react';

let dataLayer = window.dataLayer = window.dataLayer || [];

const Cart = ({accessToken, setCartItemNumber, currency, language}) => {

  const [items, setItems] = useState([]);
  const [cartMessage, setCartMessage] = useState(undefined);
  const [isCartOrderable, setIsCartOrderable] = useState(false)
  const [totalValue, setTotalValue] = useState(0);
  const [cartId, setCartId] = useState(Cookies.get(conf.cookies.cartId));
  const [directOrderCartURL, setDirectOrderCartURL] = useState('');
  const auth = useAuth();
  const history = useHistory();

  const updateElements = useCallback(() => {
    util.serviceCallWrapper({
          method: 'GET',
          url: conf.urls.cartOrchestration + '/' + cartId,
          headers: accessToken
              ? {
                Authorization: `Bearer ${accessToken}`,
                "Accept-Language": language.code,
              }
              : {"Accept-Language": language.code},
        },
        (result) => {

          let fetchedItems = result.data.items.map(itemIn => {
            return {
              key: Math.random() * 100,
              id: itemIn.id,
              articleId: itemIn.articleId,
              availability: itemIn.availability,
              name: itemIn.name,
              orderable: itemIn.orderable,
              price: itemIn.price,
              unitPrice: itemIn.unitPrice,
              quantity: itemIn.quantity,
              product: itemIn.product,
              messages: itemIn.messages,
            };
          });
          setDirectOrderCartURL(
              conf.urls.cartOrchestration + '/' + cartId + '/items');
          setItems(fetchedItems);
          setCartMessage(result.data.messages);
          setIsCartOrderable(result.data.orderable);
          setTotalValue(result.data.total);
          setCartItemNumber(result.data.items.length);
        },
        {},
        () => {
        },
        false,
        true && currency
    )
  }, [accessToken, cartId, setCartItemNumber, currency, language]);

  useEffect(() => {
    document.title = conf.pageTitles.cart;
    util.retrieveCart(auth, () => {
    }, updateElements, accessToken);

  }, [accessToken, updateElements, auth]);

  useEffect(() => {
    document.title = conf.pageTitles.cart;
    dataLayer.push({event: 'pageview'});
    dataLayer.push({event: 'gtm.dom'});
    dataLayer.push({event: 'gtm.load'});
  }, []);

  const handleDeleteClick = (event, item) => {
    util.serviceCallWrapper({
          method: 'DELETE',
          url: directOrderCartURL + '/' + item.id,
          headers: accessToken
              ? {
                Authorization: `Bearer ${accessToken}`,
                "Accept-Language": language.code,
              } : {"Accept-Language": language.code},
        },
        () => updateElements(),
        {
          200: {
            'SUCCESS': "Item " + util.displayItemDesignator(item)
                + " is removed from the cart."
          },
          404: {
            'ERROR': 'Item ' + util.displayItemDesignator(item)
                + ' is not present in the cart!'
          },
        }
    );
  };

  const handleDecreaseClick = (event, item) => {
    const itemList = items.slice();

    itemList.forEach((i) => {
      if (i.id === item.id) {
        Object.assign(i, {quantity: Number(item.quantity) - 1});

        util.serviceCallWrapper({
              method: 'PATCH',
              url: directOrderCartURL,
              data: i,
              headers: accessToken
                  ? {
                    Authorization: `Bearer ${accessToken}`,
                    "Accept-Language": language.code,
                  } : {"Accept-Language": language.code},
            },
            updateElements,
            {
              200: {
                'SUCCESS': 'Item ' + util.displayItemDesignator(item)
                    + ' quantity is decreased.'
              },
              404: {
                'ERROR': 'Item ' + util.displayItemDesignator(item)
                    + ' to be updated is not found!'
              },
              422: {
                'ERROR': 'Mandatory data to update the item '
                    + util.displayItemDesignator(item) + ' is not present!'
              },
              424: {
                'ERROR': 'Not enough stock for given item '
                    + util.displayItemDesignator(item) + '!'
              },
            }
        );
      }
    });
  };

  const handleIncreaseClick = (event, item) => {
    const itemList = items.slice();

    itemList.forEach((i) => {
      if (i.id === item.id) {
        Object.assign(i, {quantity: Number(item.quantity) + 1});

        util.serviceCallWrapper({
              method: 'PATCH',
              url: directOrderCartURL,
              data: i,
              headers: accessToken
                  ? {
                    Authorization: `Bearer ${accessToken}`,
                    "Accept-Language": language.code,
                  } : {"Accept-Language": language.code},
            },
            updateElements,
            {
              200: {
                'SUCCESS': 'Item ' + util.displayItemDesignator(item)
                    + ' quantity is increased.'
              },
              404: {
                'ERROR': 'Item ' + util.displayItemDesignator(item)
                    + ' to be updated is not found!'
              },
              422: {
                'ERROR': 'Mandatory data to update the item '
                    + util.displayItemDesignator(item) + ' is not present!'
              },
              424: {
                'ERROR': 'Not enough stock for given item '
                    + util.displayItemDesignator(item) + '!'
              },
            }
        );
      }
    });
  };

  const handleQuantityChange = (event, item) => {
    const itemList = items.slice();
    setItems(itemList.map((i) => {
      if (i.id === item.id) {
        return Object.assign(i, {quantity: Number(event.target.value)});
      }
      return i;
    }));
  };

  const handleBlurEvent = (event, item) => {
    const itemList = items.slice();

    itemList.forEach((i) => {
      if (i.id === item.id) {
        Object.assign(i, {quantity: Number(event.target.value)});

        util.serviceCallWrapper({
              method: 'PATCH',
              url: directOrderCartURL,
              data: i,
              headers: accessToken
                  ? {
                    Authorization: `Bearer ${accessToken}`,
                    "Accept-Language": language.code,
                  } : {"Accept-Language": language.code},
            },
            updateElements,
            {
              200: {
                'SUCCESS': 'Item ' + util.displayItemDesignator(item)
                    + ' quantity is changed.'
              },
              404: {
                'ERROR': 'Item ' + util.displayItemDesignator(item)
                    + ' to be updated is not found!'
              },
              422: {
                'ERROR': 'Mandatory data to update the item '
                    + util.displayItemDesignator(item) + ' is not present!'
              },
              424: {
                'ERROR': 'Not enough stock for given item '
                    + util.displayItemDesignator(item) + '!'
              },
            }
        );
      }
    });
  };

  const handleEnterPressedEvent = (event, item) => {
    const code = event.keyCode || event.which;
    if (code === 13) { //13 is the enter keycode
      const itemList = items.slice();
      itemList.forEach((i) => {
        if (i.id === item.id) {

          util.serviceCallWrapper({
                method: 'PATCH',
                url: directOrderCartURL,
                data: i,
                headers: accessToken
                    ? {
                      Authorization: `Bearer ${accessToken}`,
                      "Accept-Language": language.code,
                    } : {"Accept-Language": language.code},
              },
              updateElements,
              {
                200: {
                  'SUCCESS': 'Item ' + util.displayItemDesignator(item)
                      + ' quantity is changed.'
                },
                404: {
                  'ERROR': 'Item ' + util.displayItemDesignator(item)
                      + ' to be updated is not found!'
                },
                422: {
                  'ERROR': 'Mandatory data to update the item '
                      + util.displayItemDesignator(item) + ' is not present!'
                },
                424: {
                  'ERROR': 'Not enough stock for given item '
                      + util.displayItemDesignator(item) + '!'
                },
              }
          );
        }
      });
    }
  };

  const redirectToCheckout = () => {
    if (accessToken === null) {
      auth.signIn();
    } else {
      history.push(conf.urls.checkout);
    }
  };

  const renderItem = (item) => {
    return (<Item key={item.key} item={item}
                  handleDelete={(e) => handleDeleteClick(e, item)}
                  handleQuantity={(e) => handleQuantityChange(e, item)}
                  handleDecrease={(e) => handleDecreaseClick(e, item)}
                  handleIncrease={(e) => handleIncreaseClick(e, item)}
                  handleBlur={(e) => handleBlurEvent(e, item)}
                  handleEnterPressed={(e) => handleEnterPressedEvent(e, item)}
    />);
  };

  return (

      <Container fluid className="cart">

        <div className="cartList">
          {/*page name row  */}
          <Row>
            <Col>
              {/*<h2>Place for breadcrumb</h2>*/}
              <h2>{""}</h2>
            </Col>
          </Row>

          <Container fluid>
            {/*header row*/}
            <Row className="header">
              <Col md={{span: 3, offset: 2}}
                   className="product label">Product</Col>
              <Col md={2} className="articleNumber label">Art.Nr.</Col>
              <Col md={1} className="availability label">Availability</Col>
              <Col md={1} className="price label text-right">Price</Col>
              <Col md={2} className="subtotal label text-right">Subtotal</Col>
              <Col md={1} className="delete label"></Col>
            </Row>
            {items.map((item) => renderItem(item))}
          </Container>
        </div>

        <div className="summary">
          <Row className="header">
            <Col>Order Summary</Col>
          </Row>
          <Row xs={2} className="subtotal">
            <Col className="mobileLabel">Subtotal</Col>
            <Col className="mobileValue">{util.displayPrice(totalValue,
                currency)}</Col>
          </Row>
          <Row xs={2} className="shipping">
            <Col className="mobileLabel">Shipping</Col>
            <Col className="mobileValue">-</Col>
          </Row>
          <Row xs={2} className="tax">
            <Col className="mobileLabel">Tax</Col>
            <Col className="mobileValue">-</Col>
          </Row>
          <Row className="delimiter">
            <Col>
              <hr/>
            </Col>
          </Row>
          <Row xs={2} className="total">
            <Col className="mobileLabel">Total</Col>
            <Col className="mobileValue">{util.displayPrice(totalValue,
                currency)}</Col>
          </Row>
          <Row className="pay">
            <Col>
              {items.length && isCartOrderable ?
                  <Button variant="primary" type="button"

                          onClick={redirectToCheckout}>Proceed to
                    Checkout</Button>
                  :
                  <Button variant="primary" type="button"
                          disabled
                          onClick={redirectToCheckout}>Proceed to
                    Checkout</Button>
              }


            </Col>
          </Row>
        </div>

      </Container>
  );
};

const mapStateToProps = state => {
  return {
    accessToken: state.addAuthData.accessToken,
    currency: state.currency,
    language: state.language
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCartItemNumber: (cartItemNumber) => dispatch(
        setCurrentCartItemNumber(cartItemNumber)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);