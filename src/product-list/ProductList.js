import React, {useCallback, useEffect, useState} from 'react';
import ProductListItem from './ProductListItem';
import Cookies from 'js-cookie';
import conf from '../config';
import {Col, Container, Row, Table} from 'react-bootstrap';
import {connect} from 'react-redux';
import {useHistory} from "react-router-dom";
import 'react-toastify/dist/ReactToastify.css'
import util from '../util';
import {useAuth} from 'oidc-react';
import {setCurrentCartItemNumber} from "../redux/actions/cartItemNumber";
import {sha256} from "js-sha256";
import RecommendedItem from "./RecommendedItem";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

let dataLayer = window.dataLayer = window.dataLayer || [];

const ProductList = ({accessToken, userInfo, currency, setCartItemNumber}) => {

  const auth = useAuth();
  const [catalog, setCatalog] = useState([]);
  const [multiVariantProducts, setMultiVariantProducts] = useState([]);
  const history = useHistory();
  const [recommendations, setRecommendations] = useState(undefined);
  const responsive = {
    desktop: {
      breakpoint: {max: 3000, min: 1024},
      items: 3,
      slidesToSlide: 3 // optional, default to 1.
    },
    tablet: {
      breakpoint: {max: 1024, min: 400},
      items: 1
    },
    mobile: {
      breakpoint: {max: 768, min: 0},
      items: 1
    }

  };

  const getCatalog = useCallback(() => {
    if (currency) {
      util.serviceCallWrapper({
            method: 'GET',
            url: conf.urls.catalogOrchestration,
            params: {
              currencyId: currency.id
            },
            headers: accessToken
                ? {Authorization: `Bearer ${accessToken}`} : {}
          },
          (result) => {
            setCatalog(result.data.items.filter(product => product.type != 'VARIANT'));
            let variantsArray = [];
            result.data.items
              .filter(product => product.type == 'MULTI_VARIANT_PRODUCT')
              .forEach(multiVariantProduct => {
                const variants = result.data.items.filter(product => product.parentId === multiVariantProduct.id);
                variantsArray.push({[multiVariantProduct.id]: variants})
              })
              setMultiVariantProducts(variantsArray);

            dataLayer.push({event: 'gtm.load'});
          },
          {}
      );
    }
  }, [accessToken, currency]);

  const getRecommendations = useCallback(() => {
    const currencyId = currency ? currency.id : undefined;
    util.serviceCallWrapper({
          method: 'GET',
          url: conf.urls.recommendationOrchestration
              + '/recommendations/categories/123?length=6&currencyId=' + currencyId,
          headers: accessToken
              ? {Authorization: `Bearer ${accessToken}`} : {}
        },
        (result) => {
          setRecommendations(result.data);
        },
        {},
        undefined,
        false,
    );
  }, [accessToken, currency]);

  const handleAddToCart = (event, item) => {
    const article = {
      'articleId': item.id,
      'quantity': 1,
      'price': item.prices ? item.prices[0].unitPrice : null,
      'unitPrice': item.prices ? item.prices[0].unitPrice : null,
    };
    const cartId = Cookies.get(
        conf.cookies.cartId
    );
    util.serviceCallWrapper(
        {
          method: 'POST',
          url: conf.urls.cartOrchestration + '/' + cartId + '/items',
          data: article,
          headers: accessToken
              ? {Authorization: `Bearer ${accessToken}`}
              : {},
        },
        (result) => {
          setCartItemNumber(result.data.items.length);
          let user = userInfo ? sha256(userInfo.email) : undefined;
          let userEmail = userInfo ? userInfo.email : undefined;
          let categoryId = '123'
          dataLayer.push({
            event: 'recommendation',
            categoryId: categoryId,
            articleId: item.id,
            action: 'add_to_cart',
            userId: user,
            cartId: cartId,
            quantity: 1,
            brand: "VELOX",
            'eventCallback': function () {
              util.serviceCallWrapper(
                  {
                    method: 'POST',
                    url: conf.urls.recommendationOrchestration + '/trackings',
                    data: {
                      categoryId: categoryId,
                      articleId: item.id,
                      action: 'add_to_cart',
                      userId: userEmail,
                      cartId: cartId,
                      quantity: 1,
                      brand: "VELOX"
                    },
                    headers: accessToken
                        ? {Authorization: `Bearer ${accessToken}`}
                        : {},
                  },
                  (result) => {
                  }
              );
            }
          });

        },
        {
          201: {
            'SUCCESS': 'Item ' + item.name + ' is added to the cart.'
          },
          409: {
            'ERROR': 'Item ' + item.name + ' is already in the cart!'
          },
        });
  };

  useEffect(() => {
    getCatalog()
    getRecommendations()
  }, [accessToken, getCatalog, getRecommendations]);

  useEffect(() => {
    document.title = conf.pageTitles.plp;
    dataLayer.push({event: 'pageview'});
    dataLayer.push({event: 'gtm.dom'});
  }, []);

  const renderItem = (item) => {
    return (<ProductListItem key={Math.random()} item={item} history={history}
                             variants={multiVariantProducts.filter(variant => variant.hasOwnProperty(item.id)).length > 0 ?
                                       multiVariantProducts.filter(variant => variant.hasOwnProperty(item.id))[0][item.id] :
                                       []
                                      }
                             handleAddToCart={handleAddToCart} accessToken={accessToken} userInfo={userInfo}
    />);
  };

  const renderRecommendation = (recommendation) => {
    return (<RecommendedItem key={Math.random()} recommendation={recommendation}
                             history={history}
                             accessToken={accessToken} userInfo={userInfo}
    />);
  };
  const redirectToCheckout = () => {
    if (accessToken === null) {
      auth.signIn();
    } else {
      history.push(conf.urls.checkout);
    }
  };

  return (
      <Container fluid className="plp">

        <div className="recommendation">
          <div className="articles">
            {recommendations ?
                <div>
                  <div className="recommendationTitle">
                    <b>Current bestsellers</b>
                  </div>
                  <Carousel
                      swipeable={true}
                      draggable={true}
                      showDots={true}
                      responsive={responsive}
                      ssr={true} // means to render carousel on server-side.
                      infinite={true}
                      keyBoardControl={true}
                      customTransition="all .5"
                      transitionDuration={500}
                      containerClass="carousel-container"
                      removeArrowOnDeviceType={["tablet", "mobile"]}
                      dotListClass="custom-dot-list-style"
                      itemClass="carousel-item-padding-40-px">
                    {recommendations.articles.map(
                        (recommendation) => renderRecommendation(
                            recommendation))}
                  </Carousel>
                </div>
                :
                ""
            }
          </div>
        </div>
        <div className="itemList">
          {/*page name row  */}
          <Row>
            <Col>
              {/*<h2>Place for breadcrumb</h2>*/}
              <h2>{""}</h2>
            </Col>
          </Row>




          {/*header row*/}
          <Row className="header">
            <Col md={{span: 4, offset: 2}}
                 className="product label">Product</Col>
            <Col md={2} className="articleNumber label">Availability</Col>
            <Col md={2} className="price label">Price</Col>
            <Col md={2} className="buy label">Buy</Col>
          </Row>
          <Row>
            <Col xs={1}> </Col>
            <Col xs={10} className="mobileTitle">Products</Col>

            <Col xs={1}></Col>

          </Row>
          {catalog.map((item) => renderItem(item))}
          <Row xs={2} className="shipping">
            <Col colSpan="1" className="image"/>
          </Row>
        </div>
      </Container>

  );
};

const mapStateToProps = state => {
  return {
    accessToken: state.addAuthData.accessToken,
    userInfo: state.addAuthData.userInfo,
    currency: state.currency
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCartItemNumber: (cartItemNumber) => dispatch(
        setCurrentCartItemNumber(cartItemNumber)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
