import {LANGUAGE} from "../actionTypes";

const initialState = {
  label: localStorage.getItem('i18nextLng') && localStorage.getItem('i18nextLng') === 'de' ? 'Deutsch' : 'English',
  value: localStorage.getItem('i18nextLng') && localStorage.getItem('i18nextLng') === 'de' ? 'DE' : 'EN',
  code: localStorage.getItem('i18nextLng') || 'en',
}

export default (state = initialState, action) => {
  switch (action.type) {
    case LANGUAGE:
      return action.payload;
    default:
      return state;
  }
}
