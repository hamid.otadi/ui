import {ORDER_NUMBER} from '../actionTypes';

const initialState = {
  orderNumber: 0
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ORDER_NUMBER:
      return {
        orderNumber: action.payload.orderNumber
      }
    default:
      return state;
  }
}