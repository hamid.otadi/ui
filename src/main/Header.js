import React, {useCallback, useEffect, useState} from 'react';
import {Badge, Button, Nav, Navbar, NavDropdown, Container} from 'react-bootstrap';
import {connect} from 'react-redux';
import conf from '../config';
import {NavLink} from 'react-router-dom';
import Cookies from 'js-cookie';
import {setCurrentCartItemNumber} from '../redux/actions/cartItemNumber';
import util from '../util';
import {useAuth} from 'oidc-react';
import CurrencySwitch from "./CurrencySwitch";
import {setCurrentCurrency} from "../redux/actions/currency";
import LanguageSwitch from "./LanguageSwitch";

const Header = ({userInfo, accessToken, authenticated, isLoginPending, cartItemNumber, login, logout, setCartItemNumber, setCurrency}) => {

  const auth = useAuth();
  const [expanded, setExpanded] = useState(false);

  const getNewCart = useCallback((defaultCurrency) => {
    util.serviceCallWrapper(
{
            method: 'POST',
            url: conf.urls.cartOrchestration,
            data: { currencyId: defaultCurrency.id },
            headers: accessToken
                ? {Authorization: `Bearer ${accessToken}`} : {},
          },
        (result) => {
            Cookies.set(conf.cookies.cartId, result.data.id);
            setCartItemNumber(0);
        },
        {}
    );
  }, [accessToken, setCartItemNumber]);

  const loadDefaultCurrency = useCallback(() => {
    util.serviceCallWrapper(
        {
          method: 'GET',
          url: conf.urls.priceOrchestration + '/currencies',
          headers: accessToken
              ? {Authorization: `Bearer ${accessToken}`} : {},
        },
        (result) => {
          setCurrency(result.data.content[0]);
          getNewCart(result.data.content[0]);
        },
        {},
        () => {
        },
        false
    );
  }, [accessToken, setCurrency, getNewCart]);

  const loadCurrency = useCallback((currencyId) => {
    util.serviceCallWrapper(
        {
          method: 'GET',
          url: conf.urls.priceOrchestration + '/currencies/' + currencyId,
          headers: accessToken
              ? {Authorization: `Bearer ${accessToken}`} : {},
        },
        (result) => {
          setCurrency(result.data);
        },
        {},
        () => {
        },
        false
    );
  }, [accessToken, setCurrency]);

  const fetchCart = useCallback(() => {
    util.serviceCallWrapper({
      method: 'GET',
      url: conf.urls.cartOrchestration + '/' + Cookies.get(conf.cookies.cartId),
      headers: accessToken
          ? {Authorization: `Bearer ${accessToken}`}
          : {},
    },
(result) => {setCartItemNumber(result.data.items.length); Cookies.set(conf.cookies.cartId, result.data.id); loadCurrency(result.data.currencyId);},
        {},
        () => {loadDefaultCurrency();},
        false,
        false,
    );
  }, [accessToken, setCartItemNumber, loadCurrency, loadDefaultCurrency]);

  useEffect(() => {
    util.retrieveCart(auth, loadDefaultCurrency, fetchCart, accessToken);
  }, [accessToken, loadDefaultCurrency, fetchCart, auth]);

  return (
      <Container fluid className="fluidContainer">

            <Navbar expanded={expanded} fixed="top"  expand="md" className="header">
            <Navbar.Brand className="brand">
              <NavLink className="logo" to={conf.urls.home}>
                <img src="/assets/VeloxLogo-MidnightBlue.png"
                     style={{width: 218, height: 57}}
                     alt='Velox logo'/>
              </NavLink>
                <NavLink className="logoMobile" to={conf.urls.home}>
                <img src="/assets/VeloxLogo-Mobile.svg"
                     alt='Velox logo mobile'/>
              </NavLink>
              <Navbar.Toggle onClick={() => setExpanded(expanded ? false : "expanded")} aria-controls="basic-navbar-nav" />
            </Navbar.Brand>

          <Navbar.Collapse id="basic-navbar-nav" className="links">
              <Nav className="flex-md-row flex-column">
                <NavLink className="nav-link" exact activeClassName="active" onClick={() => setExpanded(false)} to={conf.urls.home}> Home</NavLink>
                <NavLink className="nav-link" exact activeClassName="active" onClick={() => setExpanded(false)}  to={conf.urls.product_list}>Products</NavLink>
                <NavLink className="nav-link" exact activeClassName="active" onClick={() => setExpanded(false)}  to={conf.urls.digital_upload}>Order Upload</NavLink>
                <a className="nav-link" href={conf.urls.about} target="_blank" onClick={() => setExpanded(false)} rel="noopener noreferrer">About</a>
              </Nav>
              <Nav className="nav language">
                <LanguageSwitch/>
              </Nav>
              <Nav className="nav currency">
                <CurrencySwitch/>
              </Nav>
              <Nav className="login nav">
                {isLoginPending ? (
                        authenticated ?
                            (
                                <NavDropdown title={userInfo.email}
                                             id="basic-nav-dropdown">
                                  <NavDropdown.Item
                                      onClick={logout}>Logout</NavDropdown.Item>
                                </NavDropdown>
                            )
                            :
                            <Button variant="primary" className="button"
                                    onClick={login}>Login</Button>
                    )
                    :
                    <Navbar.Text>
                      <img src="/assets/spinner.gif"
                           style={{width: 35, height: 35}}
                           alt=''/>
                    </Navbar.Text>
                }
              </Nav>
          </Navbar.Collapse>

            <Nav className="cartIcon">
              <NavLink to={conf.urls.cart}>
                <img src="/assets/cartIcon.svg"
                     className="icon"
                     alt=''/>
                <Badge pill variant="success" className="itemCartNumber">
                  {cartItemNumber}
                </Badge>
              </NavLink>
            </Nav>

        </Navbar>
      </Container>
  );
};

const mapStateToProps = state => {
  return {
    userInfo: state.addAuthData.userInfo,
    accessToken: state.addAuthData.accessToken,
    authenticated: state.addAuthData.authenticated,
    isLoginPending: state.addAuthData.isLoginPending,
    cartItemNumber: state.getCartItemNumber.cartItemNumber,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCartItemNumber: (cartItemNumber) => dispatch(
        setCurrentCartItemNumber(cartItemNumber)),
    setCurrency: (currency) => dispatch(
        setCurrentCurrency(currency))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);