import React, {useEffect, useState} from "react";
import {Col, Dropdown, Row} from "react-bootstrap";
import "../i18n/i18n";

const ProductVariantSwitch = ({
  item,
  variants,
  selectedVariant,
  setCurrentVariant,
  selectedVariantAttributes,
  setSelectedVariantAttributes
}) => {

  const [choiceAttributesMap, setChoiceAttributesMap] = useState(
      item.choiceAttributes);

  const handleChoiceChange = (event, choiceAttrId, choiceId) => {
    const variantAttributeKeys = Array.from(
        selectedVariantAttributes.keys()).filter(key => key !== choiceAttrId);

    let newSelectedVariant = variants.filter(v => {
      let condition = true;
      variantAttributeKeys.forEach(key => {
        condition = condition && v.attributeValues[key]
            && v.attributeValues[key].id === selectedVariantAttributes.get(key);
      })

      condition = condition && v.attributeValues[choiceAttrId]
          && v.attributeValues[choiceAttrId].id === choiceId
      return condition;
    });

    if (newSelectedVariant.length === 0) {
      newSelectedVariant = variants.filter(v => (v.attributeValues[choiceAttrId]
          && v.attributeValues[choiceAttrId].id === choiceId));
    }

    let updatedSelectedVariantAttributes = selectedVariantAttributes;
    if (!newSelectedVariant.isEmpty) {
      var keys = Object.keys(newSelectedVariant[0].attributeValues);
      var attributes = newSelectedVariant[0].attributeValues;
      keys.forEach(key =>
          updatedSelectedVariantAttributes.set(key, attributes[key].id)
      )
    }
    setSelectedVariantAttributes(updatedSelectedVariantAttributes);
    setCurrentVariant(newSelectedVariant[0]);
  }

  useEffect(() => {
    let updatedSelectedVariantAttributes = selectedVariantAttributes;
    if (selectedVariant !== undefined) {
      var keys = Object.keys(selectedVariant.attributeValues);
      var attributes = selectedVariant.attributeValues;
      keys.forEach(key =>
          updatedSelectedVariantAttributes.set(key, attributes[key].id)
      )
    }
  }, [item, selectedVariant, selectedVariantAttributes]);

  return (
      <div className="productVariantSwitch">
        {choiceAttributesMap.map(choiceAttribute => {
          return (
              <Row className="variant" key={Math.random()}>
                <Col xs={3} className="label"><p>{choiceAttribute.name}</p>
                </Col>
                <Col xs={3} className="mobileValue">
                  <Dropdown>

                    <Dropdown.Toggle id="dropdown-basic" className="main">
                      {selectedVariant.attributeValues[choiceAttribute.id]
                      !== undefined
                          ? selectedVariant.attributeValues[choiceAttribute.id].name
                          : ""}
                    </Dropdown.Toggle>
                    <Dropdown.Menu className="menu">
                      {choiceAttribute.values.map((choice) => {
                        return <Dropdown.Item className="" onClick={(event) => {
                          handleChoiceChange(event, choiceAttribute.id,
                              choice.id)
                        }} name={choiceAttribute.name} key={choice.id}
                                              value={choice.name}>{choice.name}</Dropdown.Item>
                      })}
                    </Dropdown.Menu>
                  </Dropdown>
                </Col>
              </Row>
          )
        })}
      </div>
  );
}

export default ProductVariantSwitch;
