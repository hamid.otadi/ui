import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import en from "./en.json";
import de from "./de.json";

const resources = {
  de: { common: de },
  en: { common: en },
 }

const options = {
  order: ['localStorage'],

  // keys or params to lookup language from
  lookupLocalStorage: 'i18nextLng',

  // cache user language on
  caches: ['localStorage'],
  excludeCacheFor: ['cimode'], // languages to not persist (cookie, localStorage)
}

i18n
.use(LanguageDetector)
.use(initReactI18next)
.init({
  resources,
  defaultNS: 'common',
  fallbackLng: 'en',
  supportedLngs: ['de', 'en'],
  interpolation: {
    escapeValue: false,
  },
  debug: false,
})

export default i18n

