import { Col, Form, Row} from "react-bootstrap";
import React from "react";
import { useTranslation } from 'react-i18next';

const UserForm = ({user, updateUser}) => {

  const { t } = useTranslation();

  const handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    if ([name] != 'phone') {
      user.addressDto[name] = value;
    } else {
      user[name] = value;
    }
    updateUser(user)
  }

  return (

      <div className="userForm">
        <Form>
          <Row className="mb-2">
            <Form.Group as={Col} md="6" >
              <Form.Label className='firstName'>{t('checkout.userForm.userForm.firstName')}</Form.Label>
              <Form.Control
                  id={Math.random()}
                  type="text"
                  className="checkoutInfo"
                  name="firstName"
                  value={user.addressDto["firstName"]}
                  onChange={(event) => handleInputChange(event)}
              />
            </Form.Group>
            <Form.Group as={Col} md="6" >
              <Form.Label>{t('checkout.userForm.userForm.lastName')}</Form.Label>
              <Form.Control
                  id={Math.random()}
                  type="text"
                  className="checkoutInfo"
                  name="lastName"
                  value={user.addressDto["lastName"]}
                  onChange={(event) => handleInputChange(event)}
              />
            </Form.Group>

          </Row>
          <Row className="mb-12">
            <Form.Group as={Col} md="12" >
              <p className="userAddressLabel">{t('checkout.userForm.userForm.company')}</p>
              <Form.Control
                  id={Math.random()}
                  type="text"
                  className="checkoutInfo"
                  name="company"
                  value={user.addressDto["company"]}
                  onChange={(event) => handleInputChange(event)}
              />
            </Form.Group>
          </Row>
          <Row className="mb-12">
            <Form.Group as={Col} md="6" >
              <Form.Label>{t('checkout.userForm.userForm.street')}</Form.Label>
              <Form.Control
                  id={Math.random()}
                  type="text"
                  className="checkoutInfo"
                  name="address"
                  value={user.addressDto["address"]}
                  onChange={(event) => handleInputChange(event)}

              />
            </Form.Group>
            <Form.Group as={Col} md="6" >
              <Form.Label>{t('checkout.userForm.userForm.number')}</Form.Label>
              <Form.Control
                  id={Math.random()}
                  type="text"
                  className="checkoutInfo"
                  name="zipCode"
                  value={user.addressDto["zipCode"]}
                  onChange={(event) => handleInputChange(event)}
              />
            </Form.Group>
          </Row>
          <Row className="mb-12">
            <Form.Group as={Col} md="4" >
              <Form.Label>{t('checkout.userForm.userForm.postcode')}</Form.Label>
              <Form.Control
                  id={Math.random()}
                  type="text"
                  className="checkoutInfo"
                  name="poBox"
                  value={user.addressDto["poBox"]}
                  onChange={(event) => handleInputChange(event)}

              />
            </Form.Group>
            <Form.Group as={Col} md="8" >
              <Form.Label>{t('checkout.userForm.userForm.city')}</Form.Label>
              <Form.Control
                  id={Math.random()}
                  type="text"
                  className="checkoutInfo"
                  name="city"
                  value={user.addressDto["city"]}
                  onChange={(event) => handleInputChange(event)}

              />

            </Form.Group>
          </Row>
          <Row className="mb-12">
            <Form.Group as={Col} md="6" >
              <Form.Label>{t('checkout.userForm.userForm.country')}</Form.Label>
              <Form.Control
                  id={Math.random()}
                  type="text"
                  className="checkoutInfo"
                  name="country"
                  value={user.addressDto["country"]}
                  onChange={(event) => handleInputChange(event)}

              />
            </Form.Group>
            <Form.Group as={Col} md="6" >
              <Form.Label>{t('checkout.userForm.userForm.phone')}</Form.Label>
              <Form.Control
                  id={Math.random()}
                  type="text"
                  className="checkoutInfo"
                  name="phone"
                  value={user.phone}
                  onChange={(event) => handleInputChange(event)}

              />
            </Form.Group>
          </Row>
        </Form>
      </div>

  )
}

export default UserForm