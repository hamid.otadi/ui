import {CURRENCY} from "../actionTypes";

export default (state = null, action) => {
  switch (action.type) {
    case CURRENCY:
      return action.payload;
    default:
      return state;
  }
}
