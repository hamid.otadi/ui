import React, {useEffect} from 'react';
import {Container} from 'react-bootstrap';
import conf from '../config';

let dataLayer = window.dataLayer = window.dataLayer || [];

const Home = () => {
  useEffect(() =>   {
    document.title = conf.pageTitles.home;
    dataLayer.push({event: 'pageview' });
    dataLayer.push({event: 'gtm.dom' });
    dataLayer.push({event: 'gtm.load' });
  }, []);
    return (
          <Container>
            <div className="startPage">

              <div className="container heading">
                <h1><b>Welcome to the next-gen Headless E-commerce</b></h1>
                <div className="row padding">
                  <div className="col-md-2">
                  </div>
                  <div className="col-md-10">
                    <div className="row">
                      <div className="col-md-7">
                      </div>
                      <div className="col-md-5">
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="container mainContent">
                Velox shop is a free and open-source reference
                implementation of a headless web-shop using microservice
                architecture.
                It shows how to approach the challenges and what technology
                is needed to build microservices.
                Velox shop is not a product nor a framework, it is more a
                library which shows, how to use current technology to build
                a flexible and scalable web-shop on your own.
              </div>

            </div>
          </Container>
    );
 }
 export default Home;