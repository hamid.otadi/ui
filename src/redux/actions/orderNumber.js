import {ORDER_NUMBER} from "../actionTypes";

export function setCurrentOrderNumber(orderNumber) {
  return {
    type: ORDER_NUMBER,
    payload: {orderNumber}
  }
}