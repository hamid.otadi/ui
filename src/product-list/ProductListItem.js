import React from 'react';
import {Badge, Button, Row, Col, Container} from "react-bootstrap"
import util from '../util';
import conf from '../config';
import {sha256} from 'js-sha256'

let dataLayer = window.dataLayer = window.dataLayer || [];

const ProductListItem = ({
  item,
  variants,
  accessToken,
  userInfo,
  history,
  handleAddToCart,
}) => {

  const displayAvailability = (availability) => {
    if (availability == null) {
      return (
          <Badge pill variant="secondary">
            Unknown
          </Badge>
      )
    } else if (availability.status === 'NOT_AVAILABLE') {
      if (availability.replenishmentTime > 0) {
        return (
            <Badge pill variant="warning">
              {availability.quantity} at Supplier
            </Badge>
        )
      } else {
        return (
            <Badge pill variant="danger">
              Out of Stock
            </Badge>
        )
      }
    } else if (availability.status === 'IN_STOCK') {
      return (
          <Badge pill variant="success">
            {availability.quantity} in Stock
          </Badge>
      )
    } else {
      return (
          <Badge pill variant="success">
            In Stock
          </Badge>
      )
    }
  }

  const handleOpenPdp = (item, variants) => {
    let user = userInfo ? sha256(userInfo.email)
        : undefined;
    let userEmail = userInfo ? userInfo.email : undefined;
    let categoryId = '123'
    if (item.type !== 'MULTI_VARIANT_PRODUCT') {
      dataLayer.push({
        event: 'recommendation',
        categoryId: categoryId,
        articleId: item.id,
        action: 'click',
        user: user,
        brand: "VELOX",
        'eventCallback': function () {
          util.serviceCallWrapper(
              {
                method: 'POST',
                url: conf.urls.recommendationOrchestration + '/trackings',
                data: {
                  categoryId: categoryId,
                  articleId: item.id,
                  action: 'click',
                  userId: userEmail,
                  brand: "VELOX"
                },
                headers: accessToken
                    ? {Authorization: `Bearer ${accessToken}`}
                    : {},
              },
              (result) => {
              }
          );
        }
      });
    }
    history.push({
      pathname: conf.urls.product_detail,
      state: {item: item, variants: variants}
    })
  }

  const displayAddToCartAndSelectButton = (item) => {
    if (item.orderable === false) {
      return (
          <Button variant="primary"
                  onClick={handleAddToCart} disabled>{item.type
          === 'MULTI_VARIANT_PRODUCT' ? 'Select variant'
              : 'Add to cart'}</Button>
      )
    }
    return (
        <Button variant="primary"
                onClick={(e) => {
                  item.type === 'MULTI_VARIANT_PRODUCT' ? handleOpenPdp(
                          item, variants)
                      : handleAddToCart(e, item)
                }}>
          {item.type === 'MULTI_VARIANT_PRODUCT' ? 'Select variant'
              : 'Add to cart'}
        </Button>
    )
  }

  return (
      <Container fluid>
        <Row className="item">
          <Col md={2} className="image">{item.images ? <img
                  src={item.images[0].url}
                  alt={item.id}
                  className="articleImage" onClick={() => {
                handleOpenPdp(item, variants)
              }}/> :
              <img src={"/assets/VeloxLogo-Grey.png"} alt={'velox-logo-grey'}
                   className="articlePlaceholder" onClick={() => {
                handleOpenPdp(item, variants)
              }}/>}</Col>
          <Col md={4} className="product col-content">
            <Row className="labelName">
              <Col xs={{span: 10, offset: 2}} className="name"
                   onClick={() => {
                     handleOpenPdp(item, variants)
                   }} className="name">
                {item.name}
              </Col>
            </Row>
            <Row className='description'>
              <Col className="label">
                {item.description ? <Col
                        className="description" onClick={() => {
                      handleOpenPdp(item, variants)
                    }}
                        dangerouslySetInnerHTML={{__html: item.description}}/>
                    : ""}

              </Col>
            </Row>
            <Row className="mobileImage">
              <Col>{item.images ? <img
                      src={item.images[0].url} alt={item.id}
                      className="articleImage" onClick={() => {
                    handleOpenPdp(item, variants)
                  }}/> :
                  <img src={"/assets/VeloxLogo-Grey.png"}
                       alt={item.id}
                       className="articlePlaceholder"/>}</Col>
            </Row>


            <Row className="itemInfo mobileAvailability">
              <Col>
                {item.type === 'MULTI_VARIANT_PRODUCT' ?
                    ''
                    :
                    util.displayAvailability(
                        item && item.availability
                            ? item.availability : null)
                }
              </Col>
            </Row>
          </Col>

          <Col md={1} className="articleNumber col-content">
            <Row>

              <Col className="mobileValue">{item.description ? <Col
                      dangerouslySetInnerHTML={{__html: item.description}}/>
                  : ""}
              </Col>
            </Row>
          </Col>
          <Col md={2} className="itemInfo availability col-content">
            {item.type === 'MULTI_VARIANT_PRODUCT' ?
                ''
                :
                util.displayAvailability(
                    item && item.availability
                        ? item.availability : null)
            }
          </Col>
          <Col md={2} className="itemInfo price col-content">
            <Row>
              <Col className="mobileLabel">Price</Col>
              <Col className="mobileDescription">
                {item.type === 'MULTI_VARIANT_PRODUCT' ?
                    ''
                    :
                    util.displayPricePLP(
                        item && item.prices
                            ? item.prices : null)
                }
              </Col>
            </Row>
          </Col>
          <Col md={2} className="itemInfo price col-content">
            <Row>
              <Col
                  className="Badge mobile">{displayAddToCartAndSelectButton(
                  item)}</Col>
            </Row>
          </Col>
        </Row>
      </Container>

  );
}

export default ProductListItem;