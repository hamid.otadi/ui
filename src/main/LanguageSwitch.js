import React, {useEffect} from "react";
import {connect} from 'react-redux';
import {Dropdown} from "react-bootstrap";
import {setCurrentLanguage} from "../redux/actions/language";
import "../i18n/i18n";
import i18n from "i18next";

const LanguageSwitch = ({language, setLanguage}) => {

  const handleLanguageChange = (language) => {
    setLanguage(language)
    i18n.changeLanguage(language.code);
  }

  useEffect(() => {
     i18n.changeLanguage(language.code);
  }, [language]);
  return (
      <div className="switch">
        <Dropdown className="language">
          <Dropdown.Toggle id="dropdown-basic" className="main">
            {language ? language.value : ""}
          </Dropdown.Toggle>
          <Dropdown.Menu className="menu">
          <Dropdown.Item className={language ? (language.code==="en" ? "item selected" : "item"): ""} onClick={() => handleLanguageChange({label: "English", value: "EN", code:"en"})} key={Math.random()}>English</Dropdown.Item>
          <Dropdown.Item className={language ? (language.code==="de" ? "item selected" : "item"): ""} onClick={() => handleLanguageChange({label: "Deutsch", value: "DE", code:"de"})} key={Math.random()}>Deutsch</Dropdown.Item>
          </Dropdown.Menu>
       </Dropdown>
      </div>
  );
}

const mapStateToProps = state => {
  return {
    language: state.language
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setLanguage: (language) => dispatch(setCurrentLanguage(language))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LanguageSwitch);
