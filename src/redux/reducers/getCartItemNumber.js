import {CART_ITEM_NUMBER} from '../actionTypes';

const initialState = {
  cartItemNumber: 0
}

export default (state=initialState, action) =>{
  switch (action.type) {
    case CART_ITEM_NUMBER:
      return {
        cartItemNumber: action.payload.cartItemNumber
      }
    default:
      return state;
  }
}