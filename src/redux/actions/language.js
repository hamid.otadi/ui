import {LANGUAGE} from "../actionTypes";

export function setCurrentLanguage(language) {
  return {
    type: LANGUAGE,
    payload: language
  }
}