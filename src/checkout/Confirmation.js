import React from 'react';
import {Button, Container} from 'react-bootstrap';
import {useHistory} from "react-router-dom";
import {connect} from "react-redux";
import conf from "../config";

const Confirmation = ({userInfo, orderNum}) => {
  const history = useHistory();

  const redirectHandler = () => {
    history.push(conf.urls.product_list)
  }
  return (
      <Container>
        <div className="startPage">

          <div className="container heading">
            <div className="confirmOrderText">
              <p>Thanks, we've received your order.</p>
            </div>
            <div className="confirmOrderNum">
              #{orderNum ? orderNum : ""}
            </div>
            <div className="confirmOrderEmail">
              <h3> A confirmation email has been sent to {userInfo
                  ? userInfo.email : ""}.</h3>
            </div>

            <div className="col-md-12 text-center">
              <Button variant="primary"  type="button"
                      onClick={redirectHandler}>Continue Shopping
              </Button>
            </div>
          </div>
        </div>
      </Container>
  );
}

const mapStateToProps = state => {
  return {
    userInfo: state.addAuthData.userInfo,
    orderNum: state.getOrderNumber.orderNumber,
  };
};
export default connect(mapStateToProps, null)(Confirmation);

