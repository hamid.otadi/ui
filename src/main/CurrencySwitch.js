import React, {useCallback, useEffect, useState} from "react";
import util from "../util";
import conf from "../config";
import {connect} from 'react-redux';
import {setCurrentCurrency} from "../redux/actions/currency"
import Cookies from 'js-cookie';
import {Dropdown} from "react-bootstrap";

const CurrencySwitch = ({accessToken, currency, setCurrency, language}) => {

  const [currencies, setCurrencies] = useState([])

  const updateCartCurrency = useCallback((newCurrency) => {
    util.serviceCallWrapper({
          method: 'PATCH',
          url: conf.urls.cartOrchestration + '/' + Cookies.get(conf.cookies.cartId),
          headers: accessToken ? {Authorization: `Bearer ${accessToken}`, "Accept-Language": language.code,} : {"Accept-Language": language.code},
          data: {
            id: Cookies.get(conf.cookies.cartId),
            currencyId: newCurrency.id
          },
        },
        () => {
          setCurrency(newCurrency);
        },
        {},
        () => {
        },
        false,
        false
    )
  }, [accessToken, currency]);

  const handleCurrencyChangeDropDown = (currency) => {
    currencies.forEach((availableCurrency) => {
      if (availableCurrency.id === currency.id) {
        updateCartCurrency(availableCurrency);
      }
    });
  }

  const loadCurrencies = useCallback(() => {
    util.serviceCallWrapper({
          method: 'GET',
          url: conf.urls.priceOrchestration + '/currencies',
          headers: accessToken
              ? {Authorization: `Bearer ${accessToken}`} : {},
        },
        (result) => {
          setCurrencies(result.data.content)
        },
        {},
        () => {
        },
        false
    );
  }, [accessToken, setCurrency]);

  useEffect(() => {
    loadCurrencies();
  }, [accessToken]);

  return (
      <div className="switch">
        <Dropdown className="currency">
          <Dropdown.Toggle id="dropdown-basic" className="main">
            {currency ? currency.isoCode : undefined}
          </Dropdown.Toggle>
          <Dropdown.Menu className="menu">
            {currencies.map((availableCurrency) => {
            return <Dropdown.Item className={currency ? (currency.id===availableCurrency.id ? "item selected" : "item"): ""} onClick={() => handleCurrencyChangeDropDown(availableCurrency)}key={availableCurrency.id}>{availableCurrency.isoCode}</Dropdown.Item>}
            )}
          </Dropdown.Menu>
       </Dropdown>
      </div>
  );
}

const mapStateToProps = state => {
  return {
    accessToken: state.addAuthData.accessToken,
    currency: state.currency,
    language: state.language
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrency: (currency) => dispatch(setCurrentCurrency(currency))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CurrencySwitch);
