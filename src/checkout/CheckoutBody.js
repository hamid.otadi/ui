import React, {useEffect, useState, useCallback} from 'react';
import axios from 'axios';
import conf from '../config';
import {
  Popover,
  OverlayTrigger,
  Button, Spinner, Row, Col, Container,
} from 'react-bootstrap';
import CheckoutItem from './CheckoutItem';
import UserAddress from './UserAddress';
import {connect} from 'react-redux';
import Cookies from 'js-cookie';
import util from '../util';
import {setCurrentCartItemNumber} from "../redux/actions/cartItemNumber";
import {setCurrentOrderNumber} from "../redux/actions/orderNumber";
import {useAuth} from "oidc-react";
import {useHistory} from "react-router-dom";
import {sha256} from "js-sha256";
import {Trans, useTranslation} from 'react-i18next';

let dataLayer = window.dataLayer = window.dataLayer || [];

const CheckoutBody = ({
  userInfo,
  accessToken,
  currency,
  setCartItemNumber,
  setOrderNumber
}) => {
  const {t} = useTranslation();
  const [orderDraft, setOrderDraft] = useState([]);
  const [checkoutSuccessful, setCheckoutSuccessful] = useState(false);
  const [isLoginPending, setIsLoginPending] = useState(false);
  const [isUserCreated, setIsUserCreated] = useState(false);
  const [editShipmentAddress, setEditShipmentAddress] = useState(false);
  const auth = useAuth();
  const history = useHistory();
  const [ETag, setETag] = useState('');
  const [cartDto, setCartDto] = useState(null);
  const [orderDto, setOrderDto] = useState([]);
  const [totalValue, setTotalValue] = useState(0);
  const [disabledButton, setDisabledButton] = useState(false);
  const [billingUser, setBillingUser] = useState({
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    addressDto: {
      firstName: '',
      lastName: '',
      company: '',
      address: '',
      city: '',
      zipCode: '',
      poBox: '',
      country: '',
    },
  });
  const [shippingUser, setShippingUser] = useState({
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    addressDto: {
      firstName: '',
      lastName: '',
      company: '',
      address: '',
      city: '',
      zipCode: '',
      poBox: '',
      country: '',
    },
  });

  const fetchUser = useCallback(async () => {
    //fetch the user
    let cartData = null;
    axios({
      method: 'GET',
      url: conf.urls.userOrchestration + '/' + userInfo.sub,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    }).then((results) => {

      if (results.status === 200) {
        setIsUserCreated(true);
      }
      return results.data;
    }, () => {
    }).then(data => {
      if (data !== undefined) {
        setBillingUser(JSON.parse(JSON.stringify(data)));
        setShippingUser(JSON.parse(JSON.stringify(data)));
      }
    }).then(() => {
      //fetch the cart-content for order to be created
      axios({
        method: 'GET',
        url: conf.urls.cartOrchestration + '/' + Cookies.get(
            conf.cookies.cartId),
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }).then(results => {
        cartData = results.data;
        setTotalValue(results.data.total);
        setCartDto(cartData);
        return results.data;
      }).then(() => {
        const orderId = Cookies.get(conf.cookies.orderId);
        //fetch the checkout preview and orderDto from checkout_orchestration
        axios({
          method: 'POST',
          url: conf.urls.checkoutOrchestration,
          data: cartData,
          headers: {
            Authorization: `Bearer ${accessToken}`,
            'content-type': "application/json;charset=UTF-8"
          },
        }).then(results => {
          setETag(results.headers.etag);
          setOrderNumber(results.data.orderNum)
          Cookies.set(conf.cookies.orderId, results.data.id);
          return results.data;
        }).then(data => {
          let items = data.entries.map((itemIn, index) => {
            return {
              key: Math.random() * 100,
              articleId: itemIn.articleId,
              articleName: itemIn.articleName,
              availability: itemIn.availability,
              quantity: itemIn.quantity,
              unitPrice: itemIn.unitPrice,
              price: itemIn.price,
              name: itemIn.name,
              imageURL: cartData.items[index].product.images[0].url,
            };
          });
          setOrderDraft(items);
          setOrderDto(data);
        }).catch(function (error) {
        })
      })
    })
  }, [userInfo, accessToken, setOrderNumber]);

  const handleCreateOrUpdateUser = (async) => {
    let user = billingUser;

    if (isUserCreated === false) {

      user.id = userInfo.sub;
      user.firstName = billingUser.addressDto.firstName;
      user.lastName = billingUser.addressDto.lastName;
      user.email = userInfo.email;

      //Create the user
      util.serviceCallWrapper(
          {
            method: 'POST',
            url: conf.urls.userOrchestration,
            data: user,
            headers: {Authorization: `Bearer ${accessToken}`}
          },
          (result) => {
            if (result.status === 200) {
              setIsUserCreated(true);
            }
          },
          {
            201: {
              'SUCCESS': 'User address is added!'
            },
            409: {
              'ERROR': 'User with given username: ' + user.id
                  + ' already exists!'
            },
            422: {
              'ERROR': 'Something went wrong. Please try again!'
            }
          }
      );
    } else {
      //Update the user
      util.serviceCallWrapper({
            method: 'PATCH',
            url: conf.urls.userOrchestration + '/' + user.id,
            data: user,
            headers: {Authorization: `Bearer ${accessToken}`}
          },
          () => {
          },
      );
    }
  }

  const popoverUnsuccess = (
      <Popover id="popover-main">
        <Popover.Title as="h3" className="popover-basic">Thank you for your
          order</Popover.Title>
        <Popover.Content className="popover-basic">
          Your order is being processed
        </Popover.Content>
      </Popover>
  );

  const popoverSuccess = (
      <Popover id="popover-main">
        <Popover.Title as="h3" className="popover-basic">Success</Popover.Title>
        <Popover.Content className="popover-basic">
          Your order was successfully placed
        </Popover.Content>
      </Popover>
  );

  const CheckoutButton = () => (
      checkoutSuccessful ?
          <OverlayTrigger trigger={['hover', 'hover']} placement="right"
                          overlay={popoverSuccess}>
              <span className='d-inline-block'>
              <Button variant="primary"
                      onClick={confirmOrder}>Order now</Button></span>
          </OverlayTrigger> :
          <OverlayTrigger trigger={["click", "click"]} placement="right"
                          overlay={popoverUnsuccess}>
            {disabledButton ? <Button variant="success" disabled
                                      onClick={confirmOrder}>Order now</Button>
                : <Button variant="primary" onClick={confirmOrder}>Order
                  now</Button>}
          </OverlayTrigger>
  );

  useEffect(() => {
    if (userInfo) {
      fetchUser();
      setIsLoginPending(true);
    }
  }, [userInfo, accessToken, fetchUser, currency]);

  const getNewCart = () => {
    util.serviceCallWrapper(
        {
          method: 'POST',
          url: conf.urls.cartOrchestration,
          data: {currencyId: currency.id},
          headers: accessToken
              ? {Authorization: `Bearer ${accessToken}`} : {},
        },
        (result) => {
          Cookies.set(conf.cookies.cartId, result.data.id);
          setCartItemNumber(0);
        },
        {},
        () => {
        },
        false
    );
  };

  const toggleShipmentAddress = () => {
    setEditShipmentAddress(!editShipmentAddress);
  }

  const confirmOrder = () => {
    let orderDtoUpdated = orderDto;
    orderDtoUpdated.orderStatus = "ORDERED";
    setDisabledButton(true);
    if (!editShipmentAddress) {
      orderDtoUpdated.shipmentAddress = orderDtoUpdated.billingAddress;
    }

    util.serviceCallWrapper({
          method: 'PATCH',
          url: conf.urls.checkoutOrchestration + '/' + orderDtoUpdated.id,
          data: orderDtoUpdated,
          headers: {
            Authorization: `Bearer ${accessToken}`,
            'If-Match': ETag,
          },
        },
        (result) => {
          if (result.status === 200) {
            setCheckoutSuccessful(true);
            setETag(result.headers.etag);
            getNewCart();
            setOrderNumber(orderDto.orderNum);
            Cookies.remove(conf.cookies.orderId);
            history.push(conf.urls.order_confirmation)

            let user = userInfo ? sha256(userInfo.email) : undefined;
            let categoryId = '123'
            dataLayer.push({
              event: 'recommendation',
              categoryId: categoryId,
              action: 'order',
              user: user,
              orderId: orderDtoUpdated.id,
              brand: "VELOX",
              'eventCallback': function () {
                util.serviceCallWrapper(
                    {
                      method: 'POST',
                      url: conf.urls.recommendationOrchestration
                          + '/trackings/orders/' + orderDtoUpdated.id,
                      headers: accessToken
                          ? {Authorization: `Bearer ${accessToken}`}
                          : {},
                    },
                    (result) => {
                    }
                );
              }
            });
          }
        },
        {
          200: {
            'SUCCESS': 'Order is placed.'
          },
          404: {
            'ERROR': 'Order not found!'
          },
          403: {
            'ERROR': 'You tried to change read-only parameters (e.g. a price)!'
          },
          412: {
            'ERROR': 'Order was changed after your last request. Please place order again!'
          },
          417: {
            'ERROR': 'Order could not be placed. Please try again!'
          },
          428: {
            'ERROR': 'Order could not be placed. Please try again!'
          },
          500: {
            'ERROR': 'Confirmation email could not be sent!'
          },
        }
    );
  };

  const updateAddress = (address, isBillingAddress) => {
    handleCreateOrUpdateUser();
    let orderDtoUpdated = orderDto;
    if (isBillingAddress) {
      orderDtoUpdated.billingAddress = address.addressDto
    } else {
      orderDtoUpdated.shippingAddress = address.addressDto
    }
    util.serviceCallWrapper({
          method: 'PATCH',
          url: conf.urls.checkoutOrchestration + '/' + orderDtoUpdated.id,
          data: orderDtoUpdated,
          headers: {
            Authorization: `Bearer ${accessToken}`,
            'If-Match': ETag,
          },
        },
        (result) => {
          if (result.status === 200) {
            setCheckoutSuccessful(true);
            setETag(result.headers.etag);
            setOrderDto(result.data);
            setIsUserCreated(true)
          }
        },
        {
          200: {
            'SUCCESS': 'Address is saved.'
          },
          404: {
            'ERROR': 'Address not found!'
          },
          403: {
            'ERROR': 'You tried to change read-only parameters'
          },
          412: {
            'ERROR': 'Order was changed after your last request. Please place order again!'
          },
          417: {
            'ERROR': 'Address could not be placed. Please try again!'
          },
          428: {
            'ERROR': 'Address could not be placed. Please try again!'
          },
          500: {
            'ERROR': 'Confirmation email could not be sent!'
          },
        }
    );
  };

  const renderItem = (item) => {
    return (<CheckoutItem key={item.key} item={item}/>);
  };
  const redirectToCheckout = () => {
    if (accessToken === null) {
      auth.signIn();
    } else {
      history.push(conf.urls.checkout);
    }
  };

  return (
      isLoginPending ?
          (isUserCreated ? (
                      <div>
                        <div className="checkoutForm">
                          <UserAddress key={Math.random()}
                                       billingUser={billingUser}
                                       shippingUser={shippingUser}
                                       isUserCreated={isUserCreated}
                                       fetchUser={fetchUser}
                                       updateAddress={updateAddress}
                                       toggleShipmentAddress={toggleShipmentAddress}
                                       editShipmentAddress={editShipmentAddress}
                                       buttonMessage={'Save'}/>
                        </div>

                        <div className="container checkoutSummary">
                          <h1 className="label" align='left'>{t(
                              'checkout.checkout.checkoutSummary.label')}</h1>
                          <Row className="checkoutHeader">
                            <Col xs={4} className="checkoutProduct">
                              {t('checkout.checkout.checkoutSummary.product')}
                            </Col>
                            <Col className="col-sm-2 col-4 text-center">
                              {t('checkout.checkout.checkoutSummary.quantity')}
                            </Col>
                            <Col xs={2}className="availability text-center">
                              {t('checkout.checkout.checkoutSummary.availability')}
                            </Col>
                            <Col xs={2} className="unitPrice">
                              {t('checkout.checkout.checkoutSummary.unitPrice')}
                            </Col>
                            <Col className="col-sm-2 col-4 text-right">
                              {t('checkout.checkout.checkoutSummary.subtotal')}
                            </Col>
                          </Row>
                          {orderDraft.map(
                              (item) => renderItem(item))}
                          <div className="policyText">
                            <p>
                              <Trans
                                  i18nKey="checkout.checkout.checkoutSummary.policyText" // optional -> fallbacks to defaults if not provided
                                  defaults="With this order you accept our general Terms and Conditions as well as our Privacy Policy." // optional defaultValue
                                  components={{a: <a/>}}
                              />
                            </p>

                          </div>
                          <Container fluid className="orderSummaryCheckout">
                          <div className="row amount">
                            <p className="col-sm-2 offset-sm-8  col-4 text-left">
                              {t('checkout.checkout.checkoutSummary.subtotal')}:
                            </p>
                            <div className="col-sm-2 col-8 text-right">
                              {util.displayPrice(totalValue, currency)}
                            </div>
                        </div>
                          <div className="row shipping">
                            <p className="col-sm-2 offset-sm-8 col-4 text-left">
                              {t('checkout.checkout.checkoutSummary.shipping')}:
                            </p>
                            <div className="col-sm-2 col-8 text-right">
                              -
                            </div>
                          </div>

                          <div className="row tax">
                            <p className="col-sm-2 offset-sm-8  col-4 text-left">
                              {t('checkout.checkout.checkoutSummary.tax')}:
                            </p>
                            <div className="col-sm-2 col-8 text-right">
                              -
                            </div>
                          </div>

                          <div className="row total">
                            <p className="col-sm-2 offset-sm-8  col-4 text-left">
                              {t('checkout.checkout.checkoutSummary.total')}:
                            </p>
                            <div className="col-sm-2 col-8 text-right">
                              {util.formatPrice(orderDto.totalPrice, currency)}
                            </div>
                          </div>

                          </Container>

                          <div className="row total">
                            <p className="col-sm-4">

                            </p>
                            <div className="col-sm-8 orderNowButton">
                              <CheckoutButton/>
                            </div>
                          </div>

                        </div>

                      </div>
                  ) :
                  (
                      <div>
                        <UserAddress key={Math.random()}
                                     shippingUser={shippingUser}
                                     billingUser={billingUser}
                                     isUserCreated={isUserCreated}
                                     fetchUser={fetchUser}
                                     updateAddress={updateAddress}
                                     toggleShipmentAddress={toggleShipmentAddress}
                                     editShipmentAddress={editShipmentAddress}
                                     buttonMessage={'Save'}/>
                      </div>
                  )
          ) :
          <div className='center-screen-spinner'>
            <Spinner animation="border" role="status" size="sm"/>{' '}Loading
            order data
          </div>
  );
};

const mapStateToProps = state => {
  return {
    userInfo: state.addAuthData.userInfo,
    accessToken: state.addAuthData.accessToken,
    currency: state.currency
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCartItemNumber: (cartItemNumber) => dispatch(
        setCurrentCartItemNumber(cartItemNumber)),
    setOrderNumber: (orderNumber) => dispatch(
        setCurrentOrderNumber(orderNumber)),

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutBody);