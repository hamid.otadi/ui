import {CURRENCY} from "../actionTypes";

export function setCurrentCurrency(currency) {
  return {
    type: CURRENCY,
    payload: currency
  }
}