import React, {useState} from 'react';
import {
  Button,
  Form,
  Container,
  Row,
  Col,
} from 'react-bootstrap';
import util from '../util';
import {connect} from 'react-redux';

const CartItem = ({
  item,
  currency,
  handleDelete,
  handleDecrease,
  handleEnterPressed,
  handleBlur,
  handleQuantity,
  handleIncrease
}) => {

  return <Row className="item">
    <Col md={2} className="image">{item.product && item.product.images ? <img
            src={item.product.images[0].url} alt={'product'}
            className="articleImage"/> :
        <img src={"/assets/VeloxLogo-Grey.png"} alt={'product'}
             className="articlePlaceholder"/>}</Col>
    <Col md={3} className="product col-content">
      <Row className="name">
        <Col xs={12}
             className="label">{item.name ? item.name : 'Not specified'}</Col>
        <Col>
          <Row className="closeButtonMobile">
            <Col xs={1} className="mobileAdjust"><Button className="delete"
                                                         onClick={handleDelete}>x</Button></Col>
            <Col className="message">
              {item.item && item.messages ?
                  util.displayItemMessage(item.messages, "left")
                  :
                  ""
              }
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="mobileImage">
        <Col>{item.product && item.product.images ? <img
                src={item.product.images[0].url} alt={'product'}
                className="articleImage"/> :
            <img src={"/assets/VeloxLogo-Grey.png"} alt={'product'}
                 className="articlePlaceholder"/>}</Col>
      </Row>
      <Row className="itemInfo mobileAvailability">
        <Col>{util.displayAvailability(
            item.availability)}</Col>
      </Row>
      <Row className="adjustment">
        <Col>
          <Button className="decrease"
                  onClick={handleDecrease}>-</Button>
          <Form.Group>
            <Form.Control className="input" type="text"
                          value={item.quantity}
                          onChange={handleQuantity}
                          onBlur={handleBlur}
                          onKeyPress={handleEnterPressed}
            />
          </Form.Group>
          <Button className="increase"
                  onClick={handleIncrease}>+</Button>
        </Col>
      </Row>
    </Col>
    <Col md={2} className="articleNumber col-content">
      <Row>
        <Col className="mobileLabel">Art.Nr.</Col>
        <Col className="mobileValue">{item.articleId}</Col>
      </Row>
    </Col>
    <Col md={1}
         className="itemInfo availability col-content">{util.displayAvailability(
        item.availability)}</Col>
    <Col md={1} className="itemInfo price col-content">
      <Row>
        <Col className="mobileLabel">Price</Col>
        <Col className="mobileValue">{util.displayPrice(
            item.unitPrice, currency)}</Col>
      </Row>
    </Col>
    <Col md={2} className="itemInfo price col-content">
      <Row>
        <Col className="mobileLabel">Subtotal</Col>
        <Col className="mobileValue">{util.displayPrice(
            item.price, currency)}</Col>
      </Row>
    </Col>
    <Col xs={1} className="closeButton">
      <Row className="content">
        <Col xs={1} className="button">
          <Button variant="primary" type="button"
                  onClick={handleDelete}>x</Button>
        </Col>
        <Col className="message">
          {item && item.messages ?
              util.displayItemMessage(item.messages)
              :
              ""
          }
        </Col>
      </Row>
    </Col>
  </Row>
}

const mapStateToProps = state => {
  return {
    currency: state.currency
  }
};

export default connect(mapStateToProps, null)(CartItem)