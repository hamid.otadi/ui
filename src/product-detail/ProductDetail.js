import React, {useCallback, useEffect, useState} from 'react';
import ProductDetailItem from './ProductDetailItem';
import Cookies from 'js-cookie';
import conf from '../config';
import {connect} from 'react-redux';
import {useHistory, useLocation} from "react-router-dom";
import 'react-toastify/dist/ReactToastify.css'
import util from '../util';
import {useAuth} from 'oidc-react';
import {setCurrentCartItemNumber} from "../redux/actions/cartItemNumber";
import {sha256} from "js-sha256";

let dataLayer = window.dataLayer = window.dataLayer || [];

const ProductDetail = ({
  accessToken,
  userInfo,
  currency,
  setCartItemNumber
}) => {

  const auth = useAuth();
  const [selectedVariantAttributes, setSelectedVariantAttributes] = useState(
      new Map());
  const location = useLocation();
  const history = useHistory();
  const [item, setItem] = useState(location.state.item);

  const handleAddToCart = (event, item) => {
    const article = {
      'articleId': item.id,
      'quantity': 1,
      'price': item.prices ? item.prices[0].unitPrice : null,
      'unitPrice': item.prices ? item.prices[0].unitPrice : null,
    };
    const cartId = Cookies.get(
        conf.cookies.cartId
    );
    util.serviceCallWrapper(
        {
          method: 'POST',
          url: conf.urls.cartOrchestration + '/' + Cookies.get(
              conf.cookies.cartId) + '/items',
          data: article,
          headers: accessToken
              ? {Authorization: `Bearer ${accessToken}`}
              : {},
        },
        (result) => {
          setCartItemNumber(result.data.items.length);
          let user = userInfo ? sha256(userInfo.email) : undefined;
          let userEmail = userInfo ? userInfo.email : undefined;
          let categoryId = '123'
          dataLayer.push({
            event: 'recommendation',
            categoryId: categoryId,
            articleId: item.id,
            action: 'add_to_cart',
            userId: user,
            cartId: cartId,
            quantity: 1,
            brand: "VELOX",
            'eventCallback': function () {
              util.serviceCallWrapper(
                  {
                    method: 'POST',
                    url: conf.urls.recommendationOrchestration + '/trackings',
                    data: {
                      categoryId: categoryId,
                      articleId: item.id,
                      action: 'add_to_cart',
                      userId: userEmail,
                      cartId: cartId,
                      quantity: 1,
                      brand: "VELOX"
                    },
                    headers: accessToken
                        ? {Authorization: `Bearer ${accessToken}`}
                        : {},
                  },
                  (result) => {
                  }
              );
            }
          });
        },
        {
          201: {
            'SUCCESS': 'Item ' + item.name + ' is added to the cart.'
          },
          409: {
            'ERROR': 'Item ' + item.name + ' is already in the cart!'
          },
        })
  };

  useEffect(() => {
    document.title = location.state.item.name + " | VELOX";
    dataLayer.push({event: 'pageview'});
    dataLayer.push({event: 'gtm.dom'});
  }, [accessToken, currency]);

  const renderItem = (item, variants) => {
    return (<ProductDetailItem key={Math.random()}
                               item={item}
                               variants={variants}
                               handleAddToCart={handleAddToCart}
                               selectedVariantAttributes={selectedVariantAttributes}
                               setSelectedVariantAttributes={setSelectedVariantAttributes}
                               accessToken={accessToken}
                               currency={currency}
    />);
  };

  const redirectToCheckout = () => {
    if (accessToken === null) {
      auth.signIn();
    } else {
      history.push(conf.urls.checkout);
    }
  };

  return (
      <div className="">
        <div className="container pdpPage">
          {
            <div>
              {renderItem(item, location.state.variants)}
            </div>
          }
        </div>
      </div>
  );
};

const mapStateToProps = state => {
  return {
    accessToken: state.addAuthData.accessToken,
    userInfo: state.addAuthData.userInfo,
    currency: state.currency
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCartItemNumber: (cartItemNumber) => dispatch(
        setCurrentCartItemNumber(cartItemNumber)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);
