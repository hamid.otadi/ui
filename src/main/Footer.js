import React from 'react';
import {Navbar, Row, Col} from "react-bootstrap";

const Footer = () => {

    return (
          <Navbar fixed="bottom" className="footer">
            <Row>
              <Col md={{ span:6, order:5 }}>
                <div className="links">
                  <a className="impressum" href="#impressum">Impressum</a>
                   |
                  <a className="privacyPolicy" href="#privacy_policy">Privacy Policy</a>
                   |
                  <a className="termsOfService" href="#terms_of_service">Terms of Service</a>
                </div>
              </Col>
              <Col md={{ span:3, order:1 }}>
              </Col>
              <Col md={{ span:3, order:0 }}>
                <div className="tradeMark"> &copy; Sly GmbH. All Rights Reserved.</div>
              </Col>
            </Row>
          </Navbar>
    )
}

export default Footer;

