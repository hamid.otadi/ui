import {CART_ITEM_NUMBER} from "../actionTypes";

export function setCurrentCartItemNumber(cartItemNumber) {
  return{
   type: CART_ITEM_NUMBER,
   payload: {cartItemNumber}
  }
}