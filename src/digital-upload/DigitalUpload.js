import React, {useEffect} from 'react';
import conf from '../config';
import Cookies from 'js-cookie';
import generateHash from 'random-hash';
import {connect} from 'react-redux';
import {sha256} from 'js-sha256';
import {useHistory} from "react-router-dom";
import util from '../util';
import {setCurrentCartItemNumber} from '../redux/actions/cartItemNumber';
import {useAuth} from 'oidc-react';

let dataLayer = window.dataLayer = window.dataLayer || [];

const DigitalUpload = ({userInfo, accessToken, setCartItemNumber}) => {

  const auth = useAuth();
  const history = useHistory();

  useEffect(() => {

    function getNewCart() {
      util.serviceCallWrapper(
{
            method: 'POST',
            url: conf.urls.cartOrchestration,
            headers: accessToken
                ? {Authorization: `Bearer ${accessToken}`} : {},
          },
          (results) => {Cookies.set(conf.cookies.cartId, results.data.id); setCartItemNumber(0);},
          {}
      );
    }

    function addItemToCart(item) {
      const newItem = {
        'articleId': item.artNr,
        'quantity': item.qty ? item.qty : 1
      };

      util.serviceCallWrapper({
        method: 'POST',
        url: conf.urls.cartOrchestration + '/' + Cookies.get(conf.cookies.cartId) + '/items',
        data: newItem,
        headers: accessToken
            ? {Authorization: `Bearer ${accessToken}`} : {},
      },
    () => history.push(conf.urls.cart),
{
              201: {
                'SUCCESS': 'Item ' + newItem.articleId + ' is added to the cart.'
              },
              409: {
                'ERROR': 'Item ' + newItem.articleId + ' is already in the cart!.'
              },
            }
      );
    }

    function fetchCart() {
      util.serviceCallWrapper({
        method: 'GET',
        url: conf.urls.cartOrchestration + '/' + Cookies.get(conf.cookies.cartId),
        headers: accessToken
            ? {Authorization: `Bearer ${accessToken}`}
            : {},
      },
      (result) => {setCartItemNumber(result.data.items.length); Cookies.set(conf.cookies.cartId, result.data.id)},
{},
      () => {getNewCart()},
false,
false
      );
    }

    // Digital Order Upload
    window.orderParser = {
      // matches the element ID which will host the Parser app
      rootId: 'parser',

      // unique identifier for the customer/user's
      // configuration (e.g. hash of the customerId or
      // email address)
      customerId: userInfo ? sha256(userInfo.email) : generateHash(
          {length: 20}),

      locale: 'en',

      // cart import callback
      onCartImport: function(cartJson) {
        cartJson.articles.map((item) => addItemToCart(item));
      },

      // webshop version
      shopId: conf.sly_connect.api_key,
    };

    // integrate parser script
    const script = document.createElement('script');

    // TODO edit path to parser if needed
    script.src = conf.urls.digital_parser_service;
    script.async = true;

    document.body.appendChild(script);

    util.retrieveCart(auth, getNewCart, fetchCart, accessToken);

  }, [userInfo, accessToken, history, setCartItemNumber, auth]);

    useEffect(() => {
      document.title = conf.pageTitles.digitalUpload;
      dataLayer.push({event: 'pageview'});
      dataLayer.push({event: 'gtm.dom'});
      dataLayer.push({event: 'gtm.load'});
    }, []);

  return (
      <div className='digitalUpload'>

        <div className='body'>
          <div id='parser'/>
        </div>

      </div>
  );

};

const mapStateToProps = state => {
  return {
    userInfo: state.addAuthData.userInfo,
    accessToken: state.addAuthData.accessToken,
  };
};

const mapDispatchToProps = (dispatch) => {
  return{
    setCartItemNumber:(cartItemNumber) => dispatch(setCurrentCartItemNumber(cartItemNumber))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DigitalUpload);
