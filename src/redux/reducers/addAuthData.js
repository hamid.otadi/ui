import {ADD_AUTH_DATA} from "../actionTypes";

const initialState = {
  userInfo: null,
  idToken: null,
  accessToken: null,
  authenticated: false,
  isLoginPending: false,
}

export default (state=initialState, action) =>{
  switch (action.type) {
    case ADD_AUTH_DATA:
      return {
        userInfo: action.payload.userInfo,
        idToken: action.payload.idToken,
        accessToken: action.payload.accessToken,
        authenticated: action.payload.authenticated,
        isLoginPending: action.payload.isLoginPending
      }
    default:
      return state;
  }
}