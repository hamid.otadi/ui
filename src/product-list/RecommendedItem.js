import React, {useEffect, useState, useCallback} from 'react';
import util from "../util";
import {Container, Row} from "react-bootstrap";
import {sha256} from "js-sha256";
import conf from "../config";

let dataLayer = window.dataLayer = window.dataLayer || [];

const RecommendedItem = ({recommendation, history, accessToken, userInfo}) => {

  const handleOpenPdp = (recommendation) => {
    let user = userInfo ? sha256(userInfo.email) : undefined;
    let userEmail = userInfo ? userInfo.email : undefined;
    let categoryId = '123'
    dataLayer.push({
      event: 'recommendation',
      categoryId: categoryId,
      articleId: recommendation.id,
      action: 'click',
      user: user,
      brand: "VELOX",
      'eventCallback': function () {
        util.serviceCallWrapper(
            {
              method: 'POST',
              url: conf.urls.recommendationOrchestration + '/trackings',
              data: {
                categoryId: categoryId,
                articleId: recommendation.id,
                action: 'recommendation',
                userId: userEmail,
                brand: "VELOX"
              },
              headers: accessToken
                  ? {Authorization: `Bearer ${accessToken}`}
                  : {},
            },
            (result) => {
            }
        );
      }
    });
    history.push({
      pathname: conf.urls.product_detail,
      state: {item: recommendation, variants: []}
    })
  }

  return (
      <Container fluid>
        <Row className="articleInfo">
          <img src={recommendation.images[0].url}
               alt={recommendation.id}
               className="articleImage"
               onClick={() => {
                 handleOpenPdp(recommendation)
               }}/>
        </Row>
        <Row className="articleInfo">
          {util.displayPricePLP(recommendation.prices)}
        </Row>
        <Row className="articleInfo">
          <p className="name">{recommendation.name.substring(0, 27)}</p>
        </Row>
      </Container>
  );

}

export default RecommendedItem;